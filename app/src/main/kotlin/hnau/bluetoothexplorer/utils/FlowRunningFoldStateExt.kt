package hnau.bluetoothexplorer.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.runningFold
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.withIndex

fun <I, O> StateFlow<I>.runningFoldState(
    parentScope: CoroutineScope,
    started: SharingStarted = SharingStarted.Lazily,
    createInitial: (I) -> O,
    operation: suspend (O, I) -> O,
): StateFlow<O> {
    val initial = createInitial(value)
    return this
        .withIndex()
        .runningFold(
            initial = Skippable(true, initial),
            operation = { acc, (i, value) ->
                if (i == 0) {
                    return@runningFold acc
                }
                Skippable(false, operation(acc.value, value))
            },
        )
        .filter { (skip) -> !skip }
        .map { (_, value) -> value }
        .stateIn(
            scope = parentScope,
            started = started,
            initialValue = initial,
        )
}

fun <T> StateFlow<T>.runningFoldState(
    parentScope: CoroutineScope,
    started: SharingStarted = SharingStarted.Lazily,
    operation: suspend (T, T) -> T,
): StateFlow<T> = runningFoldState(
    parentScope = parentScope,
    started = started,
    createInitial = { it },
    operation = operation,
)

private data class Skippable<T>(
    val skip: Boolean,
    val value: T,
)
