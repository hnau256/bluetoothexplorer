package hnau.bluetoothexplorer.utils.permissions

sealed interface PermissionState {

    data class Denied(
        val forever: Boolean,
    ) : PermissionState

    object Granted : PermissionState
}
