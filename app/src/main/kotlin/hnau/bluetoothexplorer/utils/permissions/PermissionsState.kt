package hnau.bluetoothexplorer.utils.permissions

import android.content.pm.PackageManager
import androidx.activity.ComponentActivity
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

@Suppress("FunctionName")
fun PermissionsState(
    activity: ComponentActivity,
    permissions: List<String>,
): StateFlow<Map<String, PermissionState>> = MutableStateFlow(
    collectDeniedPermissions(
        activity = activity,
        requestedOnce = false,
        permissions = permissions,
    ),
).apply {
    var requestPermissionLauncher: ActivityResultLauncher<Array<String>>? = null

    val requestPermissionsIfNeed = {
        value
            .filter { (_, state) ->
                when (state) {
                    is PermissionState.Denied -> !state.forever
                    PermissionState.Granted -> false
                }
            }
            .map { (permission) -> permission }
            .takeIf { it.isNotEmpty() }
            ?.toTypedArray()
            ?.let { permissionsNamesToRequest ->
                requestPermissionLauncher!!.launch(permissionsNamesToRequest)
            }
    }

    val updateAndRequestPermissionsIfNeed = {
        value = collectDeniedPermissions(
            activity = activity,
            requestedOnce = true,
            permissions = permissions,
        )
        requestPermissionsIfNeed()
    }

    requestPermissionLauncher = activity.registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions(),
    ) {
        updateAndRequestPermissionsIfNeed()
    }
    activity.lifecycleScope.launch {
        activity.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
            updateAndRequestPermissionsIfNeed()
        }
    }
}

@PublishedApi
internal fun collectDeniedPermissions(
    activity: ComponentActivity,
    requestedOnce: Boolean,
    permissions: List<String>,
): Map<String, PermissionState> = permissions.associateWith { name ->
    when (ContextCompat.checkSelfPermission(activity, name)) {
        PackageManager.PERMISSION_GRANTED -> PermissionState.Granted
        else -> PermissionState.Denied(
            forever = !requestedOnce && !ActivityCompat.shouldShowRequestPermissionRationale(activity, name),
        )
    }
}
