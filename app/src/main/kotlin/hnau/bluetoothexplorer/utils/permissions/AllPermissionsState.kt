package hnau.bluetoothexplorer.utils.permissions

enum class AllPermissionsState {
    Granted,
    DeniedTemporary,
    DeniedForever,
}

val Map<String, PermissionState>.allState: AllPermissionsState
    get() = values.fold(
        initial = AllPermissionsState.Granted,
    ) { acc, state ->
        listOf(acc, state.allState).maxBy { it.weight }
    }

private val AllPermissionsState.weight: Int
    get() = when (this) {
        AllPermissionsState.Granted -> 0
        AllPermissionsState.DeniedTemporary -> 1
        AllPermissionsState.DeniedForever -> 2
    }

private val PermissionState.allState: AllPermissionsState
    get() = when (this) {
        is PermissionState.Denied -> when (forever) {
            true -> AllPermissionsState.DeniedForever
            false -> AllPermissionsState.DeniedTemporary
        }

        PermissionState.Granted -> AllPermissionsState.Granted
    }
