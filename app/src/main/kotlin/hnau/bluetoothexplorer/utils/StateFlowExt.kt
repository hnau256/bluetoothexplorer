package hnau.bluetoothexplorer.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.withIndex

inline fun <I, O> StateFlow<I>.mapState(
    scope: CoroutineScope,
    crossinline transform: (I) -> O,
): StateFlow<O> {
    val initial = transform(value)
    return withIndex()
        .map { (i, value) ->
            when (i) {
                0 -> initial
                else -> transform(value)
            }
        }
        .stateIn(
            scope = scope,
            started = SharingStarted.Eagerly,
            initialValue = initial,
        )
}

@OptIn(ExperimentalCoroutinesApi::class)
fun <I, O> StateFlow<I>.flatMapState(
    scope: CoroutineScope,
    map: (I) -> StateFlow<O>,
): StateFlow<O> {
    val initialFlow = value.let(map)
    val initial = initialFlow.value
    return withIndex()
        .flatMapLatest { (i, value) ->
            if (i == 0) {
                return@flatMapLatest initialFlow
            }
            map(value)
        }
        .stateIn(
            scope = scope,
            started = SharingStarted.Eagerly,
            initialValue = initial,
        )
}

inline fun <A, B, R> combineState(
    scope: CoroutineScope,
    first: StateFlow<A>,
    second: StateFlow<B>,
    crossinline combinator: (A, B) -> R,
): StateFlow<R> {
    val initial = combinator(first.value, second.value)
    return combine(
        first.withIndex(),
        second.withIndex(),
    ) { (firstIndex, firstValue), (secondIndex, secondValue) ->
        if (firstIndex == 0 && secondIndex == 0) {
            return@combine initial
        }
        combinator(firstValue, secondValue)
    }
        .stateIn(
            scope = scope,
            started = SharingStarted.Eagerly,
            initialValue = initial,
        )
}
