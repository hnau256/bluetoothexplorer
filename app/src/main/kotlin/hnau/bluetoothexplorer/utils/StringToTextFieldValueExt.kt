package hnau.bluetoothexplorer.utils

import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue

fun String.toTextFieldValue() = TextFieldValue(
    text = this,
    selection = TextRange(length),
)
