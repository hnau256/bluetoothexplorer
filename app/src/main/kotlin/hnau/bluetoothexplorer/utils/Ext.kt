package hnau.bluetoothexplorer.utils

inline fun <T> T?.ifNull(
    ifNull: () -> T,
) = this ?: ifNull()

inline fun <reified O> Any?.castOrElse(
    elseAction: () -> O,
) = when (this) {
    is O -> this
    else -> elseAction()
}

inline fun <reified O> Any?.castOrNull() = this as? O

inline fun <reified O> Any?.castOrThrow() = this as O

fun <T> it(it: T) = it

inline fun <R> ifOrElse(
    predicate: Boolean,
    ifTrue: () -> R,
    ifFalse: () -> R,
): R = when (predicate) {
    true -> ifTrue()
    false -> ifFalse()
}

inline fun <R> ifOrNull(
    predicate: Boolean,
    ifTrue: () -> R,
): R? = ifOrElse(
    predicate = predicate,
    ifTrue = ifTrue,
    ifFalse = { null },
)
