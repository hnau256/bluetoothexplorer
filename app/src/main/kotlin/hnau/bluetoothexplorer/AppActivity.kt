package hnau.bluetoothexplorer

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.core.view.WindowCompat
import hnau.bluetoothexplorer.app.AppViewModel
import hnau.bluetoothexplorer.ui.AppView

class AppActivity : ComponentActivity() {

    private val viewModel: AppViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        val state = viewModel.getState(this)
        setContent { AppView(state) }
    }
}
