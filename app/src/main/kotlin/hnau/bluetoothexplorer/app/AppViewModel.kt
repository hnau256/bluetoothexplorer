package hnau.bluetoothexplorer.app

import android.Manifest
import android.app.Application
import android.os.Build
import androidx.activity.ComponentActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import hnau.bluetoothexplorer.utils.flatMapState
import hnau.bluetoothexplorer.utils.ifNull
import hnau.bluetoothexplorer.utils.permissions.AllPermissionsState
import hnau.bluetoothexplorer.utils.permissions.PermissionState
import hnau.bluetoothexplorer.utils.permissions.PermissionsState
import hnau.bluetoothexplorer.utils.permissions.allState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class AppViewModel(
    private val application: Application,
) : AndroidViewModel(
    application,
) {

    private var permissionsStateWrapper: PermissionsStateWrapper? = null

    fun getState(
        activity: ComponentActivity,
    ): StateFlow<AppState> = activity
        .currentPermissionsState
        .flatMapState(
            scope = viewModelScope,
        ) { permissionsState ->
            permissionsState.appState
        }

    private val bluetoothStateWrapper: BluetoothStateWrapper by lazy {
        BluetoothStateWrapper(
            viewModelScope = viewModelScope,
            context = application,
        )
    }

    private val locationWrapper: LocationWrapper by lazy {

        LocationWrapper(
            context = application,
            viewModelScope = viewModelScope,
            getStateWithLocation = { scope, location ->
                bluetoothStateWrapper.getAppState(scope, location)
            },
        )
    }

    private val Map<String, PermissionState>.appState: StateFlow<AppState>
        get() = when (allState) {
            AllPermissionsState.DeniedTemporary -> MutableStateFlow(AppState.NoPermissions(forever = false))
            AllPermissionsState.DeniedForever -> MutableStateFlow(AppState.NoPermissions(forever = true))
            AllPermissionsState.Granted -> locationWrapper.locationState
        }

    private val ComponentActivity.currentPermissionsState: StateFlow<Map<String, PermissionState>>
        get() {
            val newState = PermissionsState(
                activity = this,
                permissions = expectedPermissions,
            )
            return permissionsStateWrapper
                ?.apply { onNewPermissionsState(newState) }
                .ifNull {
                    PermissionsStateWrapper
                        .create(
                            scope = viewModelScope,
                            initialPermissionsState = newState,
                        )
                        .also { permissionsStateWrapper = it }
                }
                .state
        }

    companion object {

        private val expectedPermissions: List<String> = listOfNotNull(
            bluetoothConnectPermissionOrNull,
            bluetoothScanPermissionOrNull,
            bluetoothPermissionOrNull,
            Manifest.permission.ACCESS_FINE_LOCATION,
        )

        private val bluetoothScanPermissionOrNull
            get() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                Manifest.permission.BLUETOOTH_SCAN
            } else {
                null
            }

        private val bluetoothConnectPermissionOrNull
            get() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                Manifest.permission.BLUETOOTH_CONNECT
            } else {
                null
            }

        private val bluetoothPermissionOrNull
            get() = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
                Manifest.permission.BLUETOOTH
            } else {
                null
            }
    }
}
