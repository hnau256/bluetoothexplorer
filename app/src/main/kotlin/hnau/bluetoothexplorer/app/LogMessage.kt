package hnau.bluetoothexplorer.app

import java.util.Date

data class LogMessage(
    val timestamp: Date,
    val text: String,
    val level: Level,
) {

    enum class Level {
        Info, Error,
    }
}
