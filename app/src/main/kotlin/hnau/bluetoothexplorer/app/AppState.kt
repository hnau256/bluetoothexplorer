package hnau.bluetoothexplorer.app

import android.app.Activity
import androidx.compose.runtime.Immutable
import androidx.compose.ui.text.input.TextFieldValue

@Immutable
sealed interface AppState {

    @Immutable
    data class NoPermissions(
        val forever: Boolean,
    ) : AppState

    @Immutable
    object ResolvingLocation : AppState

    @Immutable
    data class UnableResolveLocation(
        val reason: Reason,
        val tryAgain: () -> Unit,
    ) : AppState {

        @Immutable
        sealed interface Reason {

            @Immutable
            object Unknown : Reason

            @Immutable
            object NoLocationProvided : Reason

            @Immutable
            data class NoAvailableGooglePlaySDK(
                val reason: GooglePlayServicesUnavailableReason,
            ) : Reason
        }
    }

    @Immutable
    object BluetoothIsUnavailable : AppState

    @Immutable
    data class BluetoothIsSwitchedOff(
        val switchOn: (Activity) -> Unit,
    ) : AppState

    @Immutable
    data class Scanning(
        val editing: Editing?,
        val messages: List<LogMessage>,
    ) : AppState {

        data class Editing(
            val editingPreferences: EditingPreferences,
            val updateUrlToSend: (TextFieldValue) -> Unit,
            val actionsResetApply: ActionsResetApply?,
        ) {

            data class ActionsResetApply(
                val reset: () -> Unit,
                val apply: (() -> Unit)?,
            )
        }
    }
}
