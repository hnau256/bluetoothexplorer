package hnau.bluetoothexplorer.app

import androidx.compose.runtime.Immutable
import androidx.compose.ui.text.input.TextFieldValue
import arrow.core.Either
import hnau.bluetoothexplorer.utils.toTextFieldValue
import java.net.MalformedURLException
import java.net.URL

@Immutable
data class EditingPreferences(
    val urlToSend: TextFieldValue,
) {

    object Errors {

        object Url
    }

    constructor(
        preferences: ScanningPreferences,
    ) : this(
        urlToSend = preferences.urlToSend.toString().toTextFieldValue(),
    )

    val preferences: Either<Errors.Url, ScanningPreferences> by lazy {

        val urlToSend = try {
            Either.Right(URL(urlToSend.text))
        } catch (ex: MalformedURLException) {
            Either.Left(Errors.Url)
        }

        urlToSend.map { url ->
            ScanningPreferences(
                urlToSend = url,
            )
        }
    }

    fun isEqualTo(
        preferences: ScanningPreferences,
    ): Boolean = when (val localPreferences = this.preferences) {
        is Either.Left -> false
        is Either.Right -> localPreferences.value == preferences
    }
}
