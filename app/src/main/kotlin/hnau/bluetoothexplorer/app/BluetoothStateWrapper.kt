package hnau.bluetoothexplorer.app

import android.annotation.SuppressLint
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import androidx.datastore.core.DataStoreFactory
import androidx.datastore.dataStoreFile
import arrow.core.Either
import hnau.bluetoothexplorer.utils.castOrNull
import hnau.bluetoothexplorer.utils.combineState
import hnau.bluetoothexplorer.utils.createChild
import hnau.bluetoothexplorer.utils.flatMapState
import hnau.bluetoothexplorer.utils.ifOrNull
import hnau.bluetoothexplorer.utils.scopedInState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException
import java.util.Date

class BluetoothStateWrapper(
    private val viewModelScope: CoroutineScope,
    private val context: Context,
) {

    private val messages =
        MutableStateFlow(emptyList<LogMessage>())

    private val preferencesDataStore = DataStoreFactory.create(
        serializer = ScanningPreferences.DataStoreSerializer,
        scope = viewModelScope.createChild(Dispatchers.IO, ::SupervisorJob),
        produceFile = { context.dataStoreFile("preferences.json") },
    )

    private val preferences: StateFlow<ScanningPreferences?> = preferencesDataStore
        .data
        .stateIn(
            scope = viewModelScope,
            initialValue = null,
            started = SharingStarted.Eagerly,
        )

    private val editingPreferences =
        MutableStateFlow<EditingPreferences?>(null)

    private val bluetoothAdapter: BluetoothAdapter? = context
        .getSystemService(Context.BLUETOOTH_SERVICE)
        .castOrNull<BluetoothManager>()
        ?.adapter

    private val bluetoothState: StateFlow<Boolean?> = bluetoothAdapter
        ?.let { adapter ->
            callbackFlow {
                val receiver = object : BroadcastReceiver() {

                    override fun onReceive(context: Context?, intent: Intent?) {
                        trySend(Unit)
                    }
                }
                context.registerReceiver(
                    receiver,
                    IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED),
                )
                awaitClose { context.unregisterReceiver(receiver) }
            }
                .map { adapter.isEnabled }
                .stateIn(
                    scope = viewModelScope,
                    started = SharingStarted.Lazily,
                    initialValue = adapter.isEnabled,
                )
        }
        ?: MutableStateFlow(null)

    init {
        viewModelScope.launch {
            val initialPreferences = preferences.filterNotNull().first()
            editingPreferences.compareAndSet(
                expect = null,
                update = EditingPreferences(initialPreferences),
            )
        }
    }

    @SuppressLint("MissingPermission")
    fun getAppState(
        scope: CoroutineScope,
        location: Location,
    ): StateFlow<AppState> = bluetoothState
        .scopedInState(parentScope = scope)
        .flatMapState(
            scope = scope,
        ) { (bluetoothStateScope, bluetoothIsEnabled) ->
            when (bluetoothIsEnabled) {
                null -> MutableStateFlow(AppState.BluetoothIsUnavailable)
                false -> MutableStateFlow(
                    AppState.BluetoothIsSwitchedOff(
                        switchOn = { activity ->
                            activity.startActivityForResult(
                                Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                                0,
                            )
                        },
                    ),
                )

                true -> doScanning(
                    bluetoothStateScope,
                    location,
                )
            }
        }

    private fun log(
        level: LogMessage.Level,
        text: String,
    ) {
        messages.update { messagesList ->
            messagesList + LogMessage(
                timestamp = Date(),
                text = text,
                level = level,
            )
        }
    }

    @SuppressLint("MissingPermission")
    private val devicesFlow = bluetoothAdapter
        ?.let { adapter ->
            callbackFlow {
                val action = BluetoothDevice.ACTION_FOUND
                val receiver = object : BroadcastReceiver() {
                    override fun onReceive(context: Context?, intent: Intent?) {
                        val source = intent
                            ?.takeIf { it.action == action }
                            ?: return
                        val bluetoothDevice = source
                            .getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                            ?: return
                        trySend(
                            ContentToSend.Device(
                                device = bluetoothDevice,
                                rssi = ifOrNull(
                                    predicate = source.hasExtra(BluetoothDevice.EXTRA_RSSI),
                                ) {
                                    source.getShortExtra(BluetoothDevice.EXTRA_RSSI, 0)
                                },
                            ),
                        )
                    }
                }
                context.registerReceiver(
                    receiver,
                    IntentFilter(action),
                )
                adapter.startDiscovery()
                log(
                    level = LogMessage.Level.Info,
                    text = "Starting scanning",
                )
                awaitClose {
                    log(
                        level = LogMessage.Level.Info,
                        text = "Finishing scanning",
                    )
                    adapter.cancelDiscovery()
                    context.unregisterReceiver(receiver)
                }
            }
                .buffer(
                    capacity = Int.MAX_VALUE,
                )
                .shareIn(
                    scope = viewModelScope,
                    started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 3000),
                )
        }

    @SuppressLint("MissingPermission")
    private fun doScanning(
        scope: CoroutineScope,
        location: Location,
    ): StateFlow<AppState.Scanning> {
        scope.launch {
            devicesFlow?.collect { device ->
                sendDevices(
                    contentToSend = ContentToSend(
                        latitude = location.latitude,
                        longitude = location.longitude,
                        device = device,
                    ),
                )
            }
        }
        val editing = combineState(
            scope = scope,
            first = preferences,
            second = editingPreferences,
        ) { preferencesOrNull, editingOrNull ->
            editingOrNull?.let { editing ->
                preferencesOrNull?.let { preferences ->
                    AppState.Scanning.Editing(
                        editingPreferences = editing,
                        actionsResetApply = when (editing.isEqualTo(preferences)) {
                            true -> null
                            false -> AppState.Scanning.Editing.ActionsResetApply(
                                reset = {
                                    editingPreferences.update {
                                        EditingPreferences(
                                            preferences,
                                        )
                                    }
                                },
                                apply = when (val newPreferences = editing.preferences) {
                                    is Either.Left -> null
                                    is Either.Right -> {
                                        {
                                            viewModelScope.launch {
                                                preferencesDataStore.updateData { newPreferences.value }
                                                log(
                                                    level = LogMessage.Level.Info,
                                                    text = "New preferences: ${newPreferences.value.json}",
                                                )
                                            }
                                        }
                                    }
                                },
                            )
                        },
                        updateUrlToSend = { newUrlToSend ->
                            editingPreferences.update { it?.copy(urlToSend = newUrlToSend) }
                        },
                    )
                }
            }
        }
        return combineState(
            scope = viewModelScope,
            first = editing,
            second = messages,
            combinator = AppState::Scanning,
        )
    }

    private val okHttp = OkHttpClient.Builder().build()

    private suspend fun sendDevices(
        contentToSend: ContentToSend,
    ) {
        val preferences = preferences.filterNotNull().first()
        val json = Json.encodeToString(
            serializer = ContentToSend.serializer(),
            value = contentToSend,
        )
        log(
            level = LogMessage.Level.Info,
            text = "Sending $json to ${preferences.urlToSend}",
        )
        val request = Request.Builder()
            .addHeader("accept", "application/json")
            .addHeader("Content-Type", "application/json")
            .url(preferences.urlToSend)
            .post(json.toRequestBody())
            .build()

        withContext(Dispatchers.IO) {
            try {
                okHttp.newCall(request).execute()
                log(
                    level = LogMessage.Level.Info,
                    text = "Devices was successfully sent",
                )
            } catch (ex: IOException) {
                log(
                    level = LogMessage.Level.Error,
                    text = "Error while sending devices: ${ex.message}",
                )
            }
        }
    }
}
