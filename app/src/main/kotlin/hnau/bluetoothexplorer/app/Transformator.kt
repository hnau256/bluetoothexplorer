package hnau.bluetoothexplorer.app

import java.util.Locale

fun main() {
    val keyPrefix = "BluetoothClass.Service."
    listOf(
        "AUDIO",
        "CAPTURE",
        "INFORMATION",
        "LE_AUDIO",
        "LIMITED_DISCOVERABILITY",
        "NETWORKING",
        "OBJECT_TRANSFER",
        "POSITIONING",
        "RENDER",
        "TELEPHONY",
    )
        .joinToString(
            separator = ",\n\n",
        ) { value ->
            """
                @SerialName("${value.toLowerCase(Locale.ROOT)}")
                ${value.toCamelCase(true)}(key = $keyPrefix$value)
            """.trimIndent()
        }
        .let(::print)
}

private fun String.toCamelCase(
    uppercaseFirst: Boolean,
) = fold(
    initial = "" to uppercaseFirst,
) { (acc, needUppercase), char ->
    if (char == '_') {
        return@fold acc to true
    }
    val newChar = when (needUppercase) {
        true -> char.uppercase()
        false -> char.lowercase()
    }
    (acc + newChar) to false
}
    .first
