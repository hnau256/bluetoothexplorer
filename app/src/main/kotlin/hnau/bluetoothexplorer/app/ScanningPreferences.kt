package hnau.bluetoothexplorer.app

import androidx.datastore.core.Serializer
import hnau.bluetoothexplorer.utils.mapper.Mapper
import hnau.bluetoothexplorer.utils.mapper.MappingKSerializer
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import kotlinx.serialization.json.encodeToStream
import java.io.InputStream
import java.io.OutputStream
import java.net.URL

@Serializable
data class ScanningPreferences(

    @SerialName("url_to_send")
    @Serializable(URLSerializer::class)
    val urlToSend: URL = defaultURL,
) {

    val json: String by lazy {
        ScanningPreferences.json.encodeToString(
            serializer = serializer(),
            value = this,
        )
    }

    companion object {
        private val json = Json {
            ignoreUnknownKeys = true
        }

        private val defaultURL = URL("https://127.0.0.1")
    }

    private object URLSerializer : MappingKSerializer<String, URL>(
        base = String.serializer(),
        mapper = Mapper(
            direct = ::URL,
            reverse = URL::toString,
        ),
    )

    @ExperimentalSerializationApi
    object DataStoreSerializer : Serializer<ScanningPreferences> {

        override val defaultValue = ScanningPreferences()

        override suspend fun readFrom(
            input: InputStream,
        ): ScanningPreferences = withContext(Dispatchers.IO) {
            json.decodeFromStream(
                deserializer = serializer(),
                stream = input,
            )
        }

        override suspend fun writeTo(
            t: ScanningPreferences,
            output: OutputStream,
        ) = withContext(Dispatchers.IO) {
            json.encodeToStream(
                serializer = serializer(),
                value = t,
                stream = output,
            )
        }
    }
}
