package hnau.bluetoothexplorer.app

enum class GooglePlayServicesUnavailableReason {
    Missing,
    Updating,
    VersionUpdateRequired,
    Disabled,
    Invalid,
}
