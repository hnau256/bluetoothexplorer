package hnau.bluetoothexplorer.app

import android.annotation.SuppressLint
import android.bluetooth.BluetoothClass
import android.bluetooth.BluetoothDevice
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ContentToSend(

    @SerialName("lat")
    val latitude: Double,

    @SerialName("long")
    val longitude: Double,

    @SerialName("device")
    val device: Device,
) {

    @Serializable
    data class Device(

        @SerialName("bond_state")
        val bondState: BondState,

        @SerialName("device_type")
        val deviceType: DeviceType,

        @SerialName("bluetooth_class")
        val bluetoothClass: Class,

        @SerialName("address")
        val address: String?,

        @SerialName("name")
        val name: String?,

        @SerialName("uuids")
        val uuids: List<String>,

        @SerialName("RSSI")
        val rssi: Short?,
    ) {

        @SuppressLint("MissingPermission")
        constructor(
            device: BluetoothDevice,
            rssi: Short?,
        ) : this(
            bondState = BondState.fromKey(device.bondState),
            deviceType = DeviceType.fromKey(device.type),
            bluetoothClass = Class(device.bluetoothClass),
            address = device.address,
            name = device.name,
            uuids = device.uuids?.map { it.toString() } ?: emptyList(),
            rssi = rssi,
        )

        enum class BondState(
            private val key: Int,
        ) {

            @SerialName("none")
            None(key = BluetoothDevice.BOND_NONE),

            @SerialName("bonded")
            Bonded(key = BluetoothDevice.BOND_BONDED),

            @SerialName("bonding")
            Bonding(key = BluetoothDevice.BOND_BONDING),
            ;

            companion object {

                fun fromKey(key: Int) =
                    values().find { it.key == key } ?: None
            }
        }

        enum class DeviceType(
            private val key: Int,
        ) {

            @SerialName("classic")
            Classic(key = BluetoothDevice.DEVICE_TYPE_CLASSIC),

            @SerialName("dual")
            Dual(key = BluetoothDevice.DEVICE_TYPE_DUAL),

            @SerialName("low_energy")
            Le(key = BluetoothDevice.DEVICE_TYPE_LE),

            @SerialName("unknown")
            Unknown(key = BluetoothDevice.DEVICE_TYPE_UNKNOWN),
            ;

            companion object {

                fun fromKey(key: Int) =
                    values().find { it.key == key } ?: Unknown
            }
        }

        @Serializable
        data class Class(

            @SerialName("major_device_class")
            val majorDeviceClass: MajorDevice,

            @SerialName("device_class")
            val deviceClass: Device,

            @SerialName("services")
            val services: Set<Service>,
        ) {

            constructor(
                bluetoothClass: BluetoothClass,
            ) : this(
                majorDeviceClass = MajorDevice.fromKey(key = bluetoothClass.majorDeviceClass),
                deviceClass = Device.fromKey(key = bluetoothClass.deviceClass),
                services = Service
                    .values()
                    .filter { service ->
                        bluetoothClass.hasService(service.key)
                    }
                    .toSet(),
            )

            enum class MajorDevice(
                private val key: Int,
            ) {

                @SerialName("unknown")
                Unknown(key = -1),

                @SerialName("audio_video")
                AudioVideo(key = BluetoothClass.Device.Major.AUDIO_VIDEO),

                @SerialName("computer")
                Computer(key = BluetoothClass.Device.Major.COMPUTER),

                @SerialName("health")
                Health(key = BluetoothClass.Device.Major.HEALTH),

                @SerialName("imaging")
                Imaging(key = BluetoothClass.Device.Major.IMAGING),

                @SerialName("misc")
                Misc(key = BluetoothClass.Device.Major.MISC),

                @SerialName("networking")
                Networking(key = BluetoothClass.Device.Major.NETWORKING),

                @SerialName("peripheral")
                Peripheral(key = BluetoothClass.Device.Major.PERIPHERAL),

                @SerialName("phone")
                Phone(key = BluetoothClass.Device.Major.PHONE),

                @SerialName("toy")
                Toy(key = BluetoothClass.Device.Major.TOY),

                @SerialName("uncategorized")
                Uncategorized(key = BluetoothClass.Device.Major.UNCATEGORIZED),

                @SerialName("wearable")
                Wearable(key = BluetoothClass.Device.Major.WEARABLE),
                ;

                companion object {

                    fun fromKey(key: Int) =
                        values().find { it.key == key } ?: Unknown
                }
            }

            enum class Device(
                private val key: Int,
            ) {

                @SerialName("unknown")
                Unknown(key = -1),

                @SerialName("audio_video_camcorder")
                AudioVideoCamcorder(key = BluetoothClass.Device.AUDIO_VIDEO_CAMCORDER),

                @SerialName("audio_video_car_audio")
                AudioVideoCarAudio(key = BluetoothClass.Device.AUDIO_VIDEO_CAR_AUDIO),

                @SerialName("audio_video_handsfree")
                AudioVideoHandsfree(key = BluetoothClass.Device.AUDIO_VIDEO_HANDSFREE),

                @SerialName("audio_video_headphones")
                AudioVideoHeadphones(key = BluetoothClass.Device.AUDIO_VIDEO_HEADPHONES),

                @SerialName("audio_video_hifi_audio")
                AudioVideoHifiAudio(key = BluetoothClass.Device.AUDIO_VIDEO_HIFI_AUDIO),

                @SerialName("audio_video_loudspeaker")
                AudioVideoLoudspeaker(key = BluetoothClass.Device.AUDIO_VIDEO_LOUDSPEAKER),

                @SerialName("audio_video_microphone")
                AudioVideoMicrophone(key = BluetoothClass.Device.AUDIO_VIDEO_MICROPHONE),

                @SerialName("audio_video_portable_audio")
                AudioVideoPortableAudio(key = BluetoothClass.Device.AUDIO_VIDEO_PORTABLE_AUDIO),

                @SerialName("audio_video_set_top_box")
                AudioVideoSetTopBox(key = BluetoothClass.Device.AUDIO_VIDEO_SET_TOP_BOX),

                @SerialName("audio_video_uncategorized")
                AudioVideoUncategorized(key = BluetoothClass.Device.AUDIO_VIDEO_UNCATEGORIZED),

                @SerialName("audio_video_vcr")
                AudioVideoVcr(key = BluetoothClass.Device.AUDIO_VIDEO_VCR),

                @SerialName("audio_video_video_camera")
                AudioVideoVideoCamera(key = BluetoothClass.Device.AUDIO_VIDEO_VIDEO_CAMERA),

                @SerialName("audio_video_video_conferencing")
                AudioVideoVideoConferencing(key = BluetoothClass.Device.AUDIO_VIDEO_VIDEO_CONFERENCING),

                @SerialName("audio_video_video_display_and_loudspeaker")
                AudioVideoVideoDisplayAndLoudspeaker(key = BluetoothClass.Device.AUDIO_VIDEO_VIDEO_DISPLAY_AND_LOUDSPEAKER),

                @SerialName("audio_video_video_gaming_toy")
                AudioVideoVideoGamingToy(key = BluetoothClass.Device.AUDIO_VIDEO_VIDEO_GAMING_TOY),

                @SerialName("audio_video_video_monitor")
                AudioVideoVideoMonitor(key = BluetoothClass.Device.AUDIO_VIDEO_VIDEO_MONITOR),

                @SerialName("audio_video_wearable_headset")
                AudioVideoWearableHeadset(key = BluetoothClass.Device.AUDIO_VIDEO_WEARABLE_HEADSET),

                @SerialName("computer_desktop")
                ComputerDesktop(key = BluetoothClass.Device.COMPUTER_DESKTOP),

                @SerialName("computer_handheld_pc_pda")
                ComputerHandheldPcPda(key = BluetoothClass.Device.COMPUTER_HANDHELD_PC_PDA),

                @SerialName("computer_laptop")
                ComputerLaptop(key = BluetoothClass.Device.COMPUTER_LAPTOP),

                @SerialName("computer_palm_size_pc_pda")
                ComputerPalmSizePcPda(key = BluetoothClass.Device.COMPUTER_PALM_SIZE_PC_PDA),

                @SerialName("computer_server")
                ComputerServer(key = BluetoothClass.Device.COMPUTER_SERVER),

                @SerialName("computer_uncategorized")
                ComputerUncategorized(key = BluetoothClass.Device.COMPUTER_UNCATEGORIZED),

                @SerialName("computer_wearable")
                ComputerWearable(key = BluetoothClass.Device.COMPUTER_WEARABLE),

                @SerialName("health_blood_pressure")
                HealthBloodPressure(key = BluetoothClass.Device.HEALTH_BLOOD_PRESSURE),

                @SerialName("health_data_display")
                HealthDataDisplay(key = BluetoothClass.Device.HEALTH_DATA_DISPLAY),

                @SerialName("health_glucose")
                HealthGlucose(key = BluetoothClass.Device.HEALTH_GLUCOSE),

                @SerialName("health_pulse_oximeter")
                HealthPulseOximeter(key = BluetoothClass.Device.HEALTH_PULSE_OXIMETER),

                @SerialName("health_pulse_rate")
                HealthPulseRate(key = BluetoothClass.Device.HEALTH_PULSE_RATE),

                @SerialName("health_thermometer")
                HealthThermometer(key = BluetoothClass.Device.HEALTH_THERMOMETER),

                @SerialName("health_uncategorized")
                HealthUncategorized(key = BluetoothClass.Device.HEALTH_UNCATEGORIZED),

                @SerialName("health_weighing")
                HealthWeighing(key = BluetoothClass.Device.HEALTH_WEIGHING),

                @SerialName("phone_cellular")
                PhoneCellular(key = BluetoothClass.Device.PHONE_CELLULAR),

                @SerialName("phone_cordless")
                PhoneCordless(key = BluetoothClass.Device.PHONE_CORDLESS),

                @SerialName("phone_isdn")
                PhoneIsdn(key = BluetoothClass.Device.PHONE_ISDN),

                @SerialName("phone_modem_or_gateway")
                PhoneModemOrGateway(key = BluetoothClass.Device.PHONE_MODEM_OR_GATEWAY),

                @SerialName("phone_smart")
                PhoneSmart(key = BluetoothClass.Device.PHONE_SMART),

                @SerialName("phone_uncategorized")
                PhoneUncategorized(key = BluetoothClass.Device.PHONE_UNCATEGORIZED),

                @SerialName("toy_controller")
                ToyController(key = BluetoothClass.Device.TOY_CONTROLLER),

                @SerialName("toy_doll_action_figure")
                ToyDollActionFigure(key = BluetoothClass.Device.TOY_DOLL_ACTION_FIGURE),

                @SerialName("toy_game")
                ToyGame(key = BluetoothClass.Device.TOY_GAME),

                @SerialName("toy_robot")
                ToyRobot(key = BluetoothClass.Device.TOY_ROBOT),

                @SerialName("toy_uncategorized")
                ToyUncategorized(key = BluetoothClass.Device.TOY_UNCATEGORIZED),

                @SerialName("toy_vehicle")
                ToyVehicle(key = BluetoothClass.Device.TOY_VEHICLE),

                @SerialName("wearable_glasses")
                WearableGlasses(key = BluetoothClass.Device.WEARABLE_GLASSES),

                @SerialName("wearable_helmet")
                WearableHelmet(key = BluetoothClass.Device.WEARABLE_HELMET),

                @SerialName("wearable_jacket")
                WearableJacket(key = BluetoothClass.Device.WEARABLE_JACKET),

                @SerialName("wearable_pager")
                WearablePager(key = BluetoothClass.Device.WEARABLE_PAGER),

                @SerialName("wearable_uncategorized")
                WearableUncategorized(key = BluetoothClass.Device.WEARABLE_UNCATEGORIZED),

                @SerialName("wearable_wrist_watch")
                WearableWristWatch(key = BluetoothClass.Device.WEARABLE_WRIST_WATCH),
                ;

                companion object {

                    fun fromKey(key: Int) =
                        values().find { it.key == key } ?: Unknown
                }
            }

            enum class Service(
                val key: Int,
            ) {

                @SerialName("audio")
                Audio(key = BluetoothClass.Service.AUDIO),

                @SerialName("capture")
                Capture(key = BluetoothClass.Service.CAPTURE),

                @SerialName("information")
                Information(key = BluetoothClass.Service.INFORMATION),

                @SerialName("limited_discoverability")
                LimitedDiscoverability(key = BluetoothClass.Service.LIMITED_DISCOVERABILITY),

                @SerialName("networking")
                Networking(key = BluetoothClass.Service.NETWORKING),

                @SerialName("object_transfer")
                ObjectTransfer(key = BluetoothClass.Service.OBJECT_TRANSFER),

                @SerialName("positioning")
                Positioning(key = BluetoothClass.Service.POSITIONING),

                @SerialName("render")
                Render(key = BluetoothClass.Service.RENDER),

                @SerialName("telephony")
                Telephony(key = BluetoothClass.Service.TELEPHONY),
            }
        }
    }
}
