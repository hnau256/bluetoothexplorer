package hnau.bluetoothexplorer.app

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import arrow.core.Either
import arrow.core.flatMap
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.Priority.PRIORITY_HIGH_ACCURACY
import com.google.android.gms.tasks.CancellationTokenSource
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.suspendCancellableCoroutine

@OptIn(ExperimentalCoroutinesApi::class)
@SuppressLint("MissingPermission")
suspend fun Context.resolveLocation(): Either<AppState.UnableResolveLocation.Reason, Location> =
    googlePlayServicesAvailability
        .mapLeft { AppState.UnableResolveLocation.Reason.NoAvailableGooglePlaySDK(it) }
        .flatMap {
            try {
                suspendCancellableCoroutine<Either<AppState.UnableResolveLocation.Reason, Location>> { continuation ->
                    val cancellationToken = CancellationTokenSource()
                    try {
                        LocationServices
                            .getFusedLocationProviderClient(this)
                            .getCurrentLocation(
                                PRIORITY_HIGH_ACCURACY,
                                cancellationToken.token,
                            )
                            .addOnCanceledListener { continuation.cancel() }
                            .addOnSuccessListener { locationOrNull: Location? ->
                                continuation.resume(
                                    when (locationOrNull) {
                                        null -> Either.Left(AppState.UnableResolveLocation.Reason.NoLocationProvided)
                                        else -> Either.Right(locationOrNull)
                                    },
                                    null,
                                )
                            }
                    } catch (ex: CancellationException) {
                        cancellationToken.cancel()
                    }
                }
            } catch (th: Throwable) {
                Either.Left(AppState.UnableResolveLocation.Reason.Unknown)
            }
        }

private val Context.googlePlayServicesAvailability: Either<GooglePlayServicesUnavailableReason, Unit>
    get() = when (GoogleApiAvailability().isGooglePlayServicesAvailable(this)) {
        ConnectionResult.SUCCESS ->
            Either.Right(Unit)

        ConnectionResult.SERVICE_DISABLED ->
            Either.Left(GooglePlayServicesUnavailableReason.Disabled)

        ConnectionResult.SERVICE_MISSING ->
            Either.Left(GooglePlayServicesUnavailableReason.Missing)

        ConnectionResult.SERVICE_UPDATING ->
            Either.Left(GooglePlayServicesUnavailableReason.Updating)

        ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED ->
            Either.Left(GooglePlayServicesUnavailableReason.VersionUpdateRequired)

        else ->
            Either.Left(GooglePlayServicesUnavailableReason.Invalid)
    }
