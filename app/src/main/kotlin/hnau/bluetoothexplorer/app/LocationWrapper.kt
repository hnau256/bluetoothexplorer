package hnau.bluetoothexplorer.app

import android.content.Context
import android.location.Location
import arrow.core.Either
import hnau.bluetoothexplorer.utils.flatMapState
import hnau.bluetoothexplorer.utils.scopedInState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update

class LocationWrapper(
    private val viewModelScope: CoroutineScope,
    private val context: Context,
    private val getStateWithLocation: (CoroutineScope, Location) -> StateFlow<AppState>,
) {

    val locationState = run {
        val defineLocationAttempt = MutableStateFlow(0)
        defineLocationAttempt
            .scopedInState(
                parentScope = viewModelScope,
            )
            .flatMapState(
                scope = viewModelScope,
            ) { (attemptScope) ->
                flow { emit(context.resolveLocation()) }
                    .flatMapLatest { errorOrLocation ->
                        when (errorOrLocation) {
                            is Either.Left -> flowOf(
                                AppState.UnableResolveLocation(
                                    reason = errorOrLocation.value,
                                    tryAgain = { defineLocationAttempt.update { it + 1 } },
                                ),
                            )

                            is Either.Right -> getStateWithLocation(
                                attemptScope,
                                errorOrLocation.value,
                            )
                        }
                    }
                    .stateIn(
                        scope = attemptScope,
                        started = SharingStarted.Eagerly,
                        initialValue = AppState.ResolvingLocation,
                    )
            }
    }
}
