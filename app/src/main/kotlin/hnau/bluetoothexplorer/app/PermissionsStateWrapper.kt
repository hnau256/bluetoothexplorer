package hnau.bluetoothexplorer.app

import hnau.bluetoothexplorer.utils.flatMapState
import hnau.bluetoothexplorer.utils.permissions.PermissionState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

data class PermissionsStateWrapper(
    val state: StateFlow<Map<String, PermissionState>>,
    val onNewPermissionsState: (StateFlow<Map<String, PermissionState>>) -> Unit,
) {

    companion object {

        fun create(
            scope: CoroutineScope,
            initialPermissionsState: StateFlow<Map<String, PermissionState>>,
        ): PermissionsStateWrapper {
            val wrapper = MutableStateFlow(initialPermissionsState)
            return PermissionsStateWrapper(
                state = wrapper.flatMapState(scope) { it },
                onNewPermissionsState = wrapper::value::set,
            )
        }
    }
}
