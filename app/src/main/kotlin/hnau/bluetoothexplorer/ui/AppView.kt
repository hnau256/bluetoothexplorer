package hnau.bluetoothexplorer.ui

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import hnau.bluetoothexplorer.app.AppState
import hnau.bluetoothexplorer.ui.utils.BluetoothExplorerTheme
import hnau.bluetoothexplorer.ui.utils.getTransitionSpecForHorizontalSlide
import hnau.bluetoothexplorer.ui.utils.widget.NavigationBarsSpacer
import hnau.bluetoothexplorer.ui.utils.widget.StatusBarsSpacer
import kotlinx.coroutines.flow.StateFlow

@Composable
fun AppView(
    state: StateFlow<AppState>,
) {
    val currentState by state.collectAsState()
    BluetoothExplorerTheme {
        AppView(currentState)
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun AppView(
    appState: AppState,
) = Scaffold(
    topBar = { StatusBarsSpacer() },
    bottomBar = { NavigationBarsSpacer() },
) { contentPadding ->
    val transition = updateTransition(
        targetState = appState,
        label = "DeliveryQuestionnaireSurveyAnswer",
    )
    transition.AnimatedContent(
        modifier = Modifier.fillMaxSize(),
        contentKey = { it.javaClass },
        transitionSpec = getTransitionSpecForHorizontalSlide {
            if (targetState.ordinal > initialState.ordinal) 1 else -1
        },
    ) { localAppState ->
        Box(
            modifier = Modifier.fillMaxSize(),
        ) {
            when (localAppState) {
                is AppState.NoPermissions -> NoPermissionsStateView(
                    contentPadding = contentPadding,
                    state = localAppState,
                )

                AppState.ResolvingLocation -> ResolvingLocationStateView(
                    contentPadding = contentPadding,
                )

                is AppState.UnableResolveLocation -> UnableResolveLocationStateView(
                    contentPadding = contentPadding,
                    state = localAppState,
                )

                AppState.BluetoothIsUnavailable -> BluetoothIsUnavailableStateView(
                    contentPadding = contentPadding,
                )

                is AppState.BluetoothIsSwitchedOff -> BluetoothIsSwitchedOffStateView(
                    contentPadding = contentPadding,
                    state = localAppState,
                )

                is AppState.Scanning -> ScanningStateView(
                    contentPadding = contentPadding,
                    state = localAppState,
                )
            }
        }
    }
}

private val AppState.ordinal
    get() = when (this) {
        is AppState.NoPermissions -> 0
        AppState.ResolvingLocation -> 1
        is AppState.UnableResolveLocation -> 2
        AppState.BluetoothIsUnavailable -> 3
        is AppState.BluetoothIsSwitchedOff -> 4
        is AppState.Scanning -> 5
    }
