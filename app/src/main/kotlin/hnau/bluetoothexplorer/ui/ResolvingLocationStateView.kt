package hnau.bluetoothexplorer.ui

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import hnau.bluetoothexplorer.R
import hnau.bluetoothexplorer.ui.utils.widget.progressindicator.ProgressIndicatorShadow

@Composable
fun ResolvingLocationStateView(
    contentPadding: PaddingValues,
) {
    ProgressIndicatorShadow(
        modifier = Modifier.padding(contentPadding),
    ) {
        Text(stringResource(R.string.state_resolving_location_title))
    }
}
