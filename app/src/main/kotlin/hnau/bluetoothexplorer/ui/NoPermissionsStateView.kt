package hnau.bluetoothexplorer.ui

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import hnau.bluetoothexplorer.R
import hnau.bluetoothexplorer.app.AppState
import hnau.bluetoothexplorer.ui.utils.widget.ErrorPanel
import hnau.bluetoothexplorer.ui.utils.widget.chip.Chip
import hnau.bluetoothexplorer.ui.utils.widget.chip.ChipStyle
import hnau.bluetoothexplorer.ui.utils.widget.contentsize.ContentSize
import hnau.bluetoothexplorer.utils.openAppSettings

@Composable
fun NoPermissionsStateView(
    state: AppState.NoPermissions,
    contentPadding: PaddingValues,
) = ErrorPanel(
    modifier = Modifier.padding(contentPadding),
    title = { Text(stringResource(R.string.state_no_permissions_title)) },
    button = when (state.forever) {
        true -> {
            {
                val context = LocalContext.current
                Chip(
                    onClick = { context.openAppSettings() },
                    size = ContentSize.Large,
                    style = ChipStyle.button,
                    content = { Text(stringResource(R.string.state_no_permissions_settings_button)) },
                )
            }
        }

        false -> null
    },
)
