package hnau.bluetoothexplorer.ui

import android.app.Activity
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import hnau.bluetoothexplorer.R
import hnau.bluetoothexplorer.app.AppState
import hnau.bluetoothexplorer.ui.utils.widget.ErrorPanel
import hnau.bluetoothexplorer.ui.utils.widget.chip.Chip
import hnau.bluetoothexplorer.ui.utils.widget.chip.ChipStyle
import hnau.bluetoothexplorer.ui.utils.widget.contentsize.ContentSize
import hnau.bluetoothexplorer.utils.castOrThrow

@Composable
fun BluetoothIsSwitchedOffStateView(
    contentPadding: PaddingValues,
    state: AppState.BluetoothIsSwitchedOff,
) = ErrorPanel(
    modifier = Modifier.padding(contentPadding),
    title = { Text(stringResource(R.string.state_bluetooth_is_switched_off_title)) },
    button = {
        val activity = LocalContext.current.castOrThrow<Activity>()
        Chip(
            onClick = { state.switchOn(activity) },
            size = ContentSize.Large,
            style = ChipStyle.button,
            content = { Text(stringResource(R.string.state_bluetooth_is_switched_off_button)) },
        )
    },
)
