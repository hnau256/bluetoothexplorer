package hnau.bluetoothexplorer.ui

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import hnau.bluetoothexplorer.R
import hnau.bluetoothexplorer.ui.utils.widget.ErrorPanel

@Composable
fun BluetoothIsUnavailableStateView(
    contentPadding: PaddingValues,
) = ErrorPanel(
    modifier = Modifier.padding(contentPadding),
    title = { Text(stringResource(R.string.state_bluetooth_is_unavailable_title)) },
)
