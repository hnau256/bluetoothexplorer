package hnau.bluetoothexplorer.ui

import androidx.compose.animation.expandHorizontally
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkHorizontally
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Card
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import arrow.core.Option
import hnau.bluetoothexplorer.R
import hnau.bluetoothexplorer.app.AppState
import hnau.bluetoothexplorer.app.LogMessage
import hnau.bluetoothexplorer.ui.utils.Dimens
import hnau.bluetoothexplorer.ui.utils.shape.HnauShape
import hnau.bluetoothexplorer.ui.utils.widget.AnimatedVisibility
import hnau.bluetoothexplorer.ui.utils.widget.Input
import hnau.bluetoothexplorer.ui.utils.widget.VerticalSpacer
import hnau.bluetoothexplorer.ui.utils.widget.chip.Chip
import hnau.bluetoothexplorer.ui.utils.widget.chip.ChipStyle
import hnau.bluetoothexplorer.ui.utils.widget.contentsize.ContentSize
import java.text.SimpleDateFormat

@Composable
fun ScanningStateView(
    contentPadding: PaddingValues,
    state: AppState.Scanning,
) = Column(
    modifier = Modifier
        .imePadding()
        .padding(contentPadding)
        .padding(horizontal = Dimens.separation),
) {
    VerticalSpacer(height = Dimens.separation)
    AnimatedVisibility(
        modifier = Modifier.fillMaxWidth(),
        value = state.editing,
        toLocal = Option.Companion::fromNullable,
    ) { editing ->
        Card(
            modifier = Modifier.fillMaxWidth(),
            elevation = 16.dp,
            shape = HnauShape(),
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(Dimens.separation),
            ) {
                Text(
                    text = stringResource(R.string.state_scanning_preferences_title),
                    style = MaterialTheme.typography.h6,
                )
                VerticalSpacer(height = Dimens.separation)
                val urlToSendFocusRequester = remember { FocusRequester() }
                Input(
                    modifier = Modifier.focusRequester(urlToSendFocusRequester),
                    value = editing.editingPreferences.urlToSend,
                    onValueChange = editing.updateUrlToSend,
                    isError = editing.editingPreferences.preferences.isLeft(),
                    label = { Text(stringResource(R.string.state_scanning_preferences_url_to_send)) },
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Uri,
                        imeAction = ImeAction.Done,
                    ),
                    keyboardActions = KeyboardActions(
                        onAny = {
                            urlToSendFocusRequester.freeFocus()
                            editing.actionsResetApply?.apply?.invoke()
                        },
                    ),
                )
                AnimatedVisibility(
                    value = editing.actionsResetApply,
                    toLocal = Option.Companion::fromNullable,
                ) { actionsResetApply ->
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = Dimens.separation),
                        horizontalArrangement = Arrangement.End,
                    ) {
                        Chip(
                            onClick = actionsResetApply.reset,
                            style = ChipStyle.chipHighlighted,
                            size = ContentSize.Large,
                            activeColor = MaterialTheme.colors.onSurface,
                            content = { Text(stringResource(R.string.state_scanning_preferences_reset)) },
                        )
                        AnimatedVisibility(
                            value = actionsResetApply.apply,
                            toLocal = Option.Companion::fromNullable,
                            enter = fadeIn() + expandHorizontally(),
                            exit = fadeOut() + shrinkHorizontally(),
                        ) { apply ->
                            Chip(
                                modifier = Modifier.padding(start = Dimens.separation),
                                onClick = apply,
                                size = ContentSize.Large,
                                style = ChipStyle.button,
                                content = { Text(stringResource(R.string.state_scanning_preferences_apply)) },
                            )
                        }
                    }
                }
            }
        }
    }
    val messagesState = remember { LazyListState() }
    LaunchedEffect(state.messages) {
        messagesState.animateScrollToItem(0)
    }
    LazyColumn(
        state = messagesState,
        modifier = Modifier
            .weight(1f)
            .fillMaxWidth(),
        contentPadding = PaddingValues(
            vertical = Dimens.separation,
        ),
        verticalArrangement = Arrangement.spacedBy(Dimens.smallSeparation),
        reverseLayout = true,
    ) {
        items(
            items = state.messages.asReversed(),
            key = { it.toString() },
        ) { message ->
            CompositionLocalProvider(
                LocalContentColor provides when (message.level) {
                    LogMessage.Level.Info -> MaterialTheme.colors.onSurface
                    LogMessage.Level.Error -> MaterialTheme.colors.error
                },
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth(),
                ) {
                    Text(
                        text = timestampFormatter
                            .format(message.timestamp),
                        modifier = Modifier
                            .fillMaxWidth(),
                    )
                    Text(
                        text = message.text,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(start = Dimens.separation),
                    )
                }
            }
        }
    }
}

private val timestampFormatter = SimpleDateFormat.getDateTimeInstance()
