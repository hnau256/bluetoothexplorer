package hnau.bluetoothexplorer.ui.utils

import androidx.compose.animation.AnimatedContentScope
import androidx.compose.animation.ContentTransform
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.tween
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.animation.with

@OptIn(ExperimentalAnimationApi::class)
fun <T> getTransitionSpecForHorizontalSlide(
    slideCoefficientProvider: AnimatedContentScope<T>.() -> Int,
): AnimatedContentScope<T>.() -> ContentTransform {
    return {
        val slideCoefficient = slideCoefficientProvider()
        val enterTransition = slideInHorizontally(
            animationSpec = tween(AnimationDurationMillis),
            initialOffsetX = { fullWidth -> slideCoefficient * fullWidth },
        )
        val exitTransition = slideOutHorizontally(
            animationSpec = tween(AnimationDurationMillis),
            targetOffsetX = { fullWidth -> slideCoefficient * -fullWidth },
        )
        enterTransition with exitTransition
    }
}

private const val AnimationDurationMillis = 500
