package hnau.bluetoothexplorer.ui.utils.widget

import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp

@Composable
fun HorizontalSpacer(
    width: Dp,
) = Spacer(
    modifier = Modifier.width(width),
)

@Composable
fun VerticalSpacer(
    height: Dp,
) = Spacer(
    modifier = Modifier.height(height),
)

@Composable
fun RowScope.ExpandedSpacer() =
    Spacer(modifier = Modifier.weight(1f))

@Composable
fun ColumnScope.ExpandedSpacer() =
    Spacer(modifier = Modifier.weight(1f))

@Composable
fun NavigationBarsSpacer() {
    Spacer(modifier = Modifier.navigationBarsPadding())
}

@Composable
fun StatusBarsSpacer() {
    Spacer(modifier = Modifier.statusBarsPadding())
}
