package hnau.bluetoothexplorer.ui.utils.widget.contentsize

enum class ContentSize {
    Small,
    Medium,
    Large,
    ;

    companion object {
        val default: ContentSize = Medium
    }
}
