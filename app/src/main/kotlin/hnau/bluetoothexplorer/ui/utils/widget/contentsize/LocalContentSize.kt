package hnau.bluetoothexplorer.ui.utils.widget.contentsize

import androidx.compose.runtime.staticCompositionLocalOf

val LocalContentSize =
    staticCompositionLocalOf { ContentSize.default }
