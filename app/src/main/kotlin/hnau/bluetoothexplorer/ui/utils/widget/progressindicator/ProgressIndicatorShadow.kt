package hnau.bluetoothexplorer.ui.utils.widget.progressindicator

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.LocalContentColor
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import hnau.bluetoothexplorer.ui.utils.Dimens

@Composable
fun ProgressIndicatorShadow(
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit,
) = Column(
    modifier = modifier
        .fillMaxSize()
        .clickable { }
        .background(MaterialTheme.colors.background.copy(alpha = 0.9f))
        .padding(Dimens.separation),
    verticalArrangement = Arrangement.spacedBy(
        space = Dimens.separation,
        alignment = Alignment.CenterVertically,
    ),
    horizontalAlignment = Alignment.CenterHorizontally,
) {
    ProgressIndicator()
    CompositionLocalProvider(
        LocalContentColor provides MaterialTheme.colors.onBackground,
        LocalTextStyle provides MaterialTheme.typography.subtitle1,
    ) {
        content()
    }
}

@Composable
fun ProgressIndicatorShadow(
    isVisible: Boolean,
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit,
) {
    AnimatedVisibility(
        visible = isVisible,
        modifier = modifier.fillMaxSize(),
        enter = fadeIn(),
        exit = fadeOut(),
    ) {
        ProgressIndicatorShadow(
            content = content,
        )
    }
}
