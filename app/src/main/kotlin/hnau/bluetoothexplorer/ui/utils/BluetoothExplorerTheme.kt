package hnau.bluetoothexplorer.ui.utils

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.colorResource
import hnau.bluetoothexplorer.R

@Composable
fun BluetoothExplorerTheme(
    content: @Composable () -> Unit,
) = MaterialTheme(
    colors = bluetoothExplorerColors,
    content = content,
)

private val bluetoothExplorerColors
    @Composable get() = when (isSystemInDarkTheme()) {
        true -> nightBluetoothExplorerColors
        false -> dayBluetoothExplorerColors
    }

private val dayBluetoothExplorerColors
    @Composable get() = lightColors(
        primary = colorResource(id = R.color.primary_day),
        primaryVariant = colorResource(id = R.color.primary_variant_day),
        onPrimary = colorResource(id = R.color.on_primary_day),
        secondary = colorResource(id = R.color.secondary_day),
        secondaryVariant = colorResource(id = R.color.secondary_variant_day),
        onSecondary = colorResource(id = R.color.on_secondary_day),
        surface = colorResource(id = R.color.surface_day),
        onSurface = colorResource(id = R.color.on_surface_day),
        error = colorResource(id = R.color.error_day),
        onError = colorResource(id = R.color.on_error_day),
        background = colorResource(id = R.color.background_day),
        onBackground = colorResource(id = R.color.on_background_day),
    )

private val nightBluetoothExplorerColors
    @Composable get() = darkColors(
        primary = colorResource(id = R.color.primary_night),
        primaryVariant = colorResource(id = R.color.primary_variant_night),
        onPrimary = colorResource(id = R.color.on_primary_night),
        secondary = colorResource(id = R.color.secondary_night),
        secondaryVariant = colorResource(id = R.color.secondary_variant_night),
        onSecondary = colorResource(id = R.color.on_secondary_night),
        surface = colorResource(id = R.color.surface_night),
        onSurface = colorResource(id = R.color.on_surface_night),
        error = colorResource(id = R.color.error_night),
        onError = colorResource(id = R.color.on_error_night),
        background = colorResource(id = R.color.background_night),
        onBackground = colorResource(id = R.color.on_background_night),
    )
