package hnau.bluetoothexplorer.ui.utils.widget.chip

import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.expandHorizontally
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkHorizontally
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.LocalContentColor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import arrow.core.Option
import hnau.bluetoothexplorer.ui.utils.widget.AnimatedVisibility
import hnau.bluetoothexplorer.ui.utils.widget.contentsize.ContentSize

internal enum class ChipSide { Leading, Trailing }

@Composable
internal fun ChipSide(
    side: ChipSide,
    size: ContentSize,
    contentColor: Color,
    content: (@Composable () -> Unit)?,
) = Row {
    ChipSideSeparator(
        targetWidth = when (content) {
            null -> size.contentEdgeSeparation / 2
            else -> when (side) {
                ChipSide.Leading -> size.sideContentEdgeSeparation
                ChipSide.Trailing -> size.contentSideContentSeparation
            }
        },
    )
    AnimatedVisibility(
        value = content,
        toLocal = Option.Companion::fromNullable,
        enter = fadeIn() + expandHorizontally(),
        exit = fadeOut() + shrinkHorizontally(),
    ) { contentLocal ->
        Box(
            modifier = Modifier
                .size(size.sideContentSize),
            contentAlignment = Alignment.Center,
        ) {
            CompositionLocalProvider(LocalContentColor provides contentColor) {
                contentLocal()
            }
        }
    }
    ChipSideSeparator(
        targetWidth = when (content) {
            null -> size.contentEdgeSeparation / 2
            else -> when (side) {
                ChipSide.Leading -> size.contentSideContentSeparation
                ChipSide.Trailing -> size.sideContentEdgeSeparation
            }
        },
    )
}

@Composable
private fun ChipSideSeparator(
    targetWidth: Dp,
) {
    val width by animateDpAsState(targetWidth)
    Spacer(modifier = Modifier.width(width))
}
