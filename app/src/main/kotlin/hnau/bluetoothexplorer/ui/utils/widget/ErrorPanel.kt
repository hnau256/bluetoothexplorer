package hnau.bluetoothexplorer.ui.utils.widget

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import arrow.core.Option
import hnau.bluetoothexplorer.ui.utils.Dimens
import hnau.bluetoothexplorer.ui.utils.widget.chip.ChipStyle
import hnau.bluetoothexplorer.ui.utils.widget.chip.LocalChipStyle
import hnau.bluetoothexplorer.ui.utils.widget.contentsize.ContentSize
import hnau.bluetoothexplorer.ui.utils.widget.contentsize.LocalContentSize

@Composable
fun ErrorPanel(
    title: @Composable () -> Unit,
    modifier: Modifier = Modifier,
    button: (@Composable () -> Unit)? = null,
) = Column(
    modifier = modifier
        .padding(horizontal = Dimens.largeSeparation)
        .fillMaxSize(),
    horizontalAlignment = Alignment.CenterHorizontally,
    verticalArrangement = Arrangement.spacedBy(
        Dimens.largeSeparation,
        Alignment.CenterVertically,
    ),
) {
    CompositionLocalProvider(
        LocalTextStyle provides MaterialTheme.typography.h6,
    ) {
        title()
    }
    AnimatedVisibility(
        value = button,
        toLocal = Option.Companion::fromNullable,
    ) { buttonLocal ->
        CompositionLocalProvider(
            LocalContentSize provides ContentSize.Large,
            LocalChipStyle provides ChipStyle.button,
        ) {
            buttonLocal()
        }
    }
}
