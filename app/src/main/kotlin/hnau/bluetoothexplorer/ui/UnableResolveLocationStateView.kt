package hnau.bluetoothexplorer.ui

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import hnau.bluetoothexplorer.R
import hnau.bluetoothexplorer.app.AppState
import hnau.bluetoothexplorer.app.GooglePlayServicesUnavailableReason
import hnau.bluetoothexplorer.ui.utils.widget.ErrorPanel
import hnau.bluetoothexplorer.ui.utils.widget.chip.Chip
import hnau.bluetoothexplorer.ui.utils.widget.chip.ChipStyle
import hnau.bluetoothexplorer.ui.utils.widget.contentsize.ContentSize

@Composable
fun UnableResolveLocationStateView(
    contentPadding: PaddingValues,
    state: AppState.UnableResolveLocation,
) = ErrorPanel(
    modifier = Modifier.padding(contentPadding),
    title = {
        val reason = stringResource(
            when (val reason = state.reason) {
                AppState.UnableResolveLocation.Reason.NoLocationProvided ->
                    R.string.state_unable_resolve_location_reason_no_location

                AppState.UnableResolveLocation.Reason.Unknown ->
                    R.string.state_unable_resolve_location_reason_unknown

                is AppState.UnableResolveLocation.Reason.NoAvailableGooglePlaySDK -> when (reason.reason) {
                    GooglePlayServicesUnavailableReason.Missing -> R.string.state_unable_resolve_location_reason_gms_missing
                    GooglePlayServicesUnavailableReason.Updating -> R.string.state_unable_resolve_location_reason_gms_updating
                    GooglePlayServicesUnavailableReason.VersionUpdateRequired -> R.string.state_unable_resolve_location_reason_gms_need_update
                    GooglePlayServicesUnavailableReason.Disabled -> R.string.state_unable_resolve_location_reason_gms_disabled
                    GooglePlayServicesUnavailableReason.Invalid -> R.string.state_unable_resolve_location_reason_gms_invalid
                }
            },
        )
        Text(stringResource(R.string.state_unable_resolve_location_title, reason))
    },
    button = {
        Chip(
            onClick = state.tryAgain,
            size = ContentSize.Large,
            style = ChipStyle.button,
            content = { Text(stringResource(R.string.state_unable_resolve_location_button)) },
        )
    },
)
